//
//  ViewController.swift
//  cobaLocation
//
//  Created by Timotius Leonardo Lianoto on 04/08/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import CloudKit

class ViewController: UIViewController, CLLocationManagerDelegate {
    let notificationCenter = UNUserNotificationCenter.current()
    var angka = 0
    var locationku = CLLocation()
    private var locationManager:CLLocationManager?
    let publicDatabase = CKContainer.default().publicCloudDatabase
    var recordIDs = [CKRecord.ID]()
    private let latLngLabel: UILabel = {
        let label = UILabel()
        label.backgroundColor = .systemFill
        label.numberOfLines = 0
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 26)
        return label
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        latLngLabel.frame = CGRect(x: 20, y: view.bounds.height / 2 - 50, width: view.bounds.width - 40, height: 100)
        view.addSubview(latLngLabel)
        getUserLocation()
        print(publicDatabase.databaseScope)
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            if !didAllow {
                print("User has declined notifications")
            }
        }
        notificationCenter.getNotificationSettings { (settings) in
          if settings.authorizationStatus != .authorized {
            
          }
        }
        
    }
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        locationku = locations.last!
        if let location = locations.last {
            latLngLabel.text = "Lat : \(location.coordinate.latitude) \nLng : \(location.coordinate.longitude)"
            print("wow")
            print(UserDefaults.standard.string(forKey: "userId"))
        }
    }
    
    func scheduleNotification(notificationType: String) {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let content = UNMutableNotificationContent()
        content.title = notificationType
        content.body = "JAUH BOS"
        content.sound = UNNotificationSound.default
        content.badge = 1
        print(angka)
        angka = angka + 1
        let identifier = "Local Notification"
        let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        notificationCenter.add(request) { (error) in
            if let error = error {
                print("Error \(error.localizedDescription)")
            }
        }
        
    }
    @IBAction func button(_ sender: Any) {
        let record = CKRecord(recordType: "NewRecordType")
        record.setValue(locationku, forKey: "location")
        print(record.recordID)
        let defaults = UserDefaults.standard
        defaults.set(record.recordID.recordName, forKey: "userId")
        print("wow \(String(describing: record.creatorUserRecordID))")
        publicDatabase.save(record) { (savedRecord, error) in
            
            if error == nil {
                
                print("Record Saved")
                
            } else {
                
                print("Record Not Saved")
                
            }
            
        }
    }
    
    @IBAction func update(_ sender: Any) {
        
        let recordID = CKRecord.ID(recordName: UserDefaults.standard.string(forKey: "userId") ?? "nil")
        
        publicDatabase.fetch(withRecordID: recordID) { (record, error) in
            
            if error == nil {
                
                record?.setValue(self.locationku, forKey: "location")
                
                self.publicDatabase.save(record!, completionHandler: { (newRecord, error) in
                    
                    if error == nil {
                        
                        print("Record Saved")
                        
                    } else {
                        
                        print("Record Not Saved")
                        
                    }
                    
                })
                
            } else {
                
                print("Could not fetch record")
                
            }
            
        }
    }
    
    @IBAction func retrieve(_ sender: Any) {
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "NewRecordType", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
        
        let operation = CKQueryOperation(query: query)
        
        
        recordIDs.removeAll()
        
        operation.recordFetchedBlock = { record in
            self.recordIDs.append(record.recordID)
            
        }
        
        operation.queryCompletionBlock = { cursor, error in
            
            DispatchQueue.main.async {
                print("RecordIDs: \(self.recordIDs)")
                
            }
            
        }
        
        publicDatabase.add(operation)
    }
    

}

