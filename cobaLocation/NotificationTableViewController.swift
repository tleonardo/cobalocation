//
//  NotificationTableViewController.swift
//  cobaLocation
//
//  Created by Timotius Leonardo Lianoto on 27/10/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import UIKit
import CloudKit

class NotificationTableViewController: UITableViewController {
    
    var pacar = [CKRecord]()
    var refresh: UIRefreshControl!
    var identifier = [CKRecord.ID]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        refresh = UIRefreshControl()
        refresh.attributedTitle = NSAttributedString(string: "Pull To Reload Pacarmu")
        refresh.addTarget(self, action: #selector(loadPacar), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        setupCloudKitSubscribtion()
        DispatchQueue.main.async {
            NotificationCenter.default.addObserver(self, selector: #selector(self.loadPacar), name: NSNotification.Name(rawValue: "performReload"), object: nil)
        }
        
        

        loadPacar()
        
        
    }
    
    func setupCloudKitSubscribtion() {
        let userDefaults = UserDefaults.standard
        
        if userDefaults.bool(forKey: "subscribed") == false {
            let query = NSPredicate(format: "%K == %@", "creatorUserRecordID" ,CKRecord.Reference(recordID: CKRecord.ID(recordName: "3B364A30-DD2F-4132-9A7C-43A20BEBA718"), action: CKRecord_Reference_Action.none))
            let subscription = CKQuerySubscription(recordType: "NewRecordType", predicate: query, subscriptionID: "ContentSubs", options: [.firesOnRecordCreation])
            let notificationInfo = CKSubscription.NotificationInfo()
            notificationInfo.titleLocalizationKey = "zzz Item Changes on %1$@"
            notificationInfo.titleLocalizationArgs = ["content"]
            notificationInfo.shouldBadge = true
            notificationInfo.soundName = "default"
            
            subscription.notificationInfo = notificationInfo
            
            let publicData = CKContainer.default().publicCloudDatabase
            
            publicData.save(subscription) { (subscription, error) in
                if error != nil {
                    print(error?.localizedDescription)
                }else {
                    userDefaults.setValue(true, forKey: "subscribed")
                    userDefaults.synchronize()
                }
            }
        }
        if userDefaults.bool(forKey: "update") == false {
            let query = NSPredicate(format: "%K == %@", "creatorUserRecordID" ,CKRecord.Reference(recordID: CKRecord.ID(recordName: "3B364A30-DD2F-4132-9A7C-43A20BEBA718"), action: CKRecord_Reference_Action.none))
            let subscription = CKQuerySubscription(recordType: "NewRecordType", predicate: query, subscriptionID: "ContentChanges2", options: [.firesOnRecordDeletion
            ])
            let notificationInfo = CKSubscription.NotificationInfo()
            notificationInfo.titleLocalizationKey = "zzz Item Changes on %1$@"
            notificationInfo.titleLocalizationArgs = ["content"]
            notificationInfo.shouldBadge = true
            notificationInfo.soundName = "default"
            
            subscription.notificationInfo = notificationInfo
            
            let publicData = CKContainer.default().publicCloudDatabase
            
            publicData.save(subscription) { (subscription, error) in
                if error != nil {
                    print(error?.localizedDescription)
                }else {
                    userDefaults.setValue(true, forKey: "update")
                    userDefaults.synchronize()
                }
            }
        }
    }
    
    @objc func loadPacar () {
        pacar = [CKRecord]()
        
        let publicData = CKContainer.default().publicCloudDatabase
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "NewRecordType", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
        
        publicData.perform(query, inZoneWith: nil) { (result, error) in
            if let items = result {
                self.pacar = items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    for item in self.pacar {
                        self.identifier.append(item.recordID)
                    }
                    self.refresh.endRefreshing()
                    self.view.layoutSubviews()
                }
            }
        }
    }
    
    func loadData() {
        pacar = [CKRecord]()
        
        let publicData = CKContainer.default().publicCloudDatabase
        let predicate = NSPredicate(value: true)
        
        let query = CKQuery(recordType: "NewRecordType", predicate: predicate)
        query.sortDescriptors = [NSSortDescriptor(key: "modificationDate", ascending: false)]
        
        publicData.perform(query, inZoneWith: nil) { (result, error) in
            if let items = result {
                self.pacar = items
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                    for item in self.pacar {
                        self.identifier.append(item.recordID)
                    }
                    self.refresh.endRefreshing()
                    self.view.layoutSubviews()
                }
            }
        }
        
    }
    
    @IBAction func addItem(_ sender: Any) {
        let alert = UIAlertController(title: "New Item", message: "Enter New Item Cuy", preferredStyle: .alert)
        alert.addTextField { (textfield: UITextField) in
            textfield.placeholder = "Nama Bebeb Kamu"
        }
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { (action:UIAlertAction) in
            let texfield = alert.textFields?.first
            if texfield?.text != "" {
                let newIem = CKRecord(recordType: "NewRecordType")
                newIem["content"] = texfield?.text
                
                let publicData = CKContainer.default().publicCloudDatabase
                publicData.save(newIem) { (savedRecord, error) in
                    
                    if error == nil {
                        DispatchQueue.main.async {
                            self.tableView.beginUpdates()
                            self.pacar.insert(newIem, at: 0)
                            let indexPath = IndexPath(row: 0, section: 0)
                            self.tableView.insertRows(at: [indexPath], with: .top)
                            self.tableView.endUpdates()
                            print("Record Saved")
                        }
                        
                    } else {
                        
                        print("Record Not Saved Because \(error)")
                        
                    }
                    
                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return pacar.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        if pacar.count == 0 {
            return cell
        }
        let item = pacar[indexPath.row]
        if let contentPacar = item["content"] as? String {
            let dateFormat = DateFormatter()
            dateFormat.dateFormat = "MM/dd/yyyy"
            let dateString = dateFormat.string(from: item.creationDate!)
            
            cell.textLabel?.text = contentPacar
            cell.detailTextLabel?.text = dateString
        }
        
        return cell
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }

    
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    

    
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let publicData = CKContainer.default().publicCloudDatabase
        let alert = UIAlertController(title: "New Item", message: "Enter New Item Cuy", preferredStyle: .alert)
        alert.addTextField { (textfield: UITextField) in
            textfield.placeholder = "Nama Bebeb Kamu"
        }
        alert.addAction(UIAlertAction(title: "Send", style: .default, handler: { (action:UIAlertAction) in
            let texfield = alert.textFields?.first
            let nama = texfield?.text
            if texfield?.text != "" {
                let recordID = CKRecord.ID(recordName: "3B364A30-DD2F-4132-9A7C-43A20BEBA718")
                publicData.fetch(withRecordID: recordID) { (record, error) in
                    if let errorMessage = error {
                        print("Record Error: \(errorMessage.localizedDescription)")
                        
                    }
                    else {
                        record?.setValue(nama, forKey: "content")
                        publicData.save(record!, completionHandler: { (newRecord, error) in
                            if let errorMessage = error {
                                print("Save Error: \(errorMessage.localizedDescription)")
                            }
                            else {
                                print("Update Saved")
                            }
                        })
                    }
                }
//                let newIem = CKRecord(recordType: "NewRecordType")
//                newIem["content"] = texfield?.text
//
//                let publicData = CKContainer.default().publicCloudDatabase
//                publicData.fetch(withRecordID: self.identifier[indexPath.row]) { (updateRecord, error) in
//                    if error == nil {
//                        updateRecord?.setObject(newIem as? CKRecordValue, forKey: "content")
//
//                        let modifyRecord = CKModifyRecordsOperation(recordsToSave: [newIem], recordIDsToDelete: nil)
//                        modifyRecord.savePolicy = CKModifyRecordsOperation.RecordSavePolicy.allKeys
//                        modifyRecord.qualityOfService = QualityOfService.userInitiated
//                                modifyRecord.modifyRecordsCompletionBlock = { savedRecords, deletedRecordIDs, error in
//                                    if error == nil {
//                                        print("Modified")
//                                    }else {
//                                        print(error ?? Error.self)
//                                    }
//                                }
//                        publicData.add(modifyRecord)
//
//                        DispatchQueue.main.async {
//                            print("Record Saved")
//                        }
//
//                    } else {
//
//                        print("Record Not Saved Because \(String(describing: error))")
//
//                    }
//                }
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

