//
//  InterfaceController.swift
//  cobaWatch Extension
//
//  Created by Timotius Leonardo Lianoto on 04/08/20.
//  Copyright © 2020 Timotius Leonardo Lianoto. All rights reserved.
//

import WatchKit
import Foundation
import CoreLocation


class InterfaceController: WKInterfaceController,CLLocationManagerDelegate {
    private var locationManager:CLLocationManager?
    @IBOutlet var langtitudeLabel: WKInterfaceLabel!
    @IBOutlet var longtitudeLabel: WKInterfaceLabel!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        getUserLocation()
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    func getUserLocation() {
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.allowsBackgroundLocationUpdates = true
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last {
            langtitudeLabel.setText("Lat : \(location.coordinate.latitude)")
            longtitudeLabel.setText("Lng : \(location.coordinate.longitude)")
        }
    }

}
